%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60

%define STDIN 0
%define STDOUT 1

%define ASCII_NULL 0x00
%define ASCII_TAB 0x09
%define ASCII_NEWLINE 0x0A
%define ASCII_SPACE 0x20
%define ASCII_MINUS '-'
%define ASCII_ZERO '0'


section .text 

; Принимает код возврата и завершает текущий процесс
exit:
	; rdi - return code
	mov	rax, SYS_EXIT		; rax <- sys exit code
	syscall				; call system exit


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	; rdi - char*
	xor	rax, rax		; rax <- 0 (string length)
.loop:					;
	cmp	byte[rdi + rax], ASCII_NULL	; if NULL - end loop
	je	.endloop		; 
	inc	rax			; length++
	jmp	.loop			; 
.endloop:				;
	ret				;


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	; rdi - char*
	push	rdi			; save string pointer
	call	string_length		; rax <- string length
	mov	rdx, rax		; rdx <- rax (str len)
	pop	rsi			; rsi <- char* (string pointer)
	mov	rax, SYS_WRITE		; rax <- sys write code
	mov	rdi, STDOUT		; rdi <- stdout descriptor
	syscall				; call system write
	ret				;

 
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov	rdi, ASCII_NEWLINE	; rdi <- newline ascii code
	; There are some tricks for use less call+ret instructions

; Принимает код символа и выводит его в stdout
print_char:
	; rdi - char code
	push	di			; push di to have char* in rsp
	mov	rax, SYS_WRITE		; rax <- sys write code
	mov	rdi, STDOUT		; rdi <- stdout descriptor
	mov	rsi, rsp		; rsi <- char* in rsp
	mov	rdx, 1			; rdi <- one char length
	syscall				; call system write
	pop	di			; pop di to save callee-saved rsp
	ret				;


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	; rdi - signed int
	cmp	rdi, 0			; if positive - print like unsigned
	jge	print_uint		;
.minus:					;
	push	rdi			; save number to the stack
	mov	rdi, ASCII_MINUS	; rdi <- minus code
	call	print_char		; print minus to stdin
	pop	rdi			; load number from stack
	neg	rdi			; num = -num (to call print_uint)
	; There are some tricks for use less call+ret instructions

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	; rdi - uint
	xor	rdx, rdx		; rdx <- 0 (remainder of division)
	mov	rax, rdi		; rax <- number to be converted
	mov	rdi, rsp		; rdi <- current stack ponter
	sub	rsp, 21			; rsp-=21 (memory allocation)
	dec	rdi			; make rdi point to allocated memory
	mov	byte[rdi], ASCII_NULL	; add NULL terminator (rdi is char* now)
	mov	rsi, 10			; rsi <- divisor (10)
.loop:					;
	div	rsi			; rax <- rax / rsi, rdx <- rax % rsi
	add	dl, ASCII_ZERO		; dl <- ascii code of remainder
	dec	rdi			; rdi-- (get new address to put char)
	mov	[rdi], dl		; mem[rdi] <- remainder ascii code
	xor	rdx, rdx		; rdx <- 0 (clear remainder)
	cmp	rax, 0			; if nothing to div - end of loop
	jne	.loop			;
.endloop:				;
	call	print_string		; print number to stdout
	add	rsp, 21			; deallocate memory
	ret				;


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	; rdi - char* first string
	; rsi - char* second string
	mov	rax, 1			; rax <- 1 (let the strings equals)
.loop:					;
	mov	r8b, byte[rdi]		; compare chars of first and second string ->
	cmp	r8b, byte[rsi]		; -> using r8b register (cannot 'cmp mem[], mem[]')
	jne	.not_equals		; if not equals - strings not equals (sadness)
	cmp	r8b, ASCII_NULL		; if chars equals null - string equals (sadness)
	je	.end			;
	inc	rdi			; inc first string pointer
	inc	rsi			; inc second string pointer
	jmp	.loop			; 
.not_equals:				;
	mov	rax, 0			; rax <- 0 (strings not equals)
.end:					;
	ret				;


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	mov	rax, SYS_READ		; rax <- sys read code
	push	ax			; push ax to have void* in rsp 
	mov	rdi, STDIN		; rdi <- stdin descriptor
	mov	rsi, rsp		; rsi <- void* buffer (rsp)
	mov	rdx, 1			; rdi <- 1 byte we will try to read
	syscall				; call system read
	cmp	rax, 1			; check how many bytes are read
	je	.success		; if 1 - success
.fail:					;
	pop	ax			; pop ax to save callee-saved rsp
	xor	rax, rax		; rax <- 0 (fail to read 1 byte)
	ret				;
.success:				;
	pop	ax			; pop ax to save callee-saved rsp
	ret				;


; read_char, но возвращает -1 если  это пробельный символ
; пробельный символ = space | tab | newline
read_char_extensible:
	call	read_char		; call read char
	cmp	rax, ASCII_SPACE	; if space - whitespace
	je	.whitespace		;
	cmp	rax, ASCII_TAB		; if tab - whitespace
	je	.whitespace		;
	cmp	rax, ASCII_NEWLINE	; if newline - whitespace
	je	.whitespace		;
	ret				; else return code
.whitespace:				;
	mov	rax, -1			; rax <- -1 (whitespace)
	ret				;


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; И ВИДИМО СЧИТЫВАЯ ДО ЛЮБОГО ПРОБЕЛЬНОГО СИМВОЛА
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	; rdi - void* buffer
	; rsi - buffer size
	push	rdi			; save buffer pointer to return it in the end
	cmp	rsi, 1			; if buffer size less than 1 - end of buffer
	jl	.end_of_buffer		;
	push	rdi			; save buffer pointer
	push	rsi			; save buffer size
.find_start:				;
	call	read_char_extensible	; call read_char_extensible
	cmp	rax, -1			; if whitespace - loop
	je	.find_start		;
	jmp	.after_read_char	; 
.loop:					;
	push	rdi			; save buffer pointer and size
	push	rsi			;
	call	read_char_extensible	; call read_char_extensible
.after_read_char:			;
	pop	rsi			; load buffer size and pointer
	pop	rdi			;
	cmp	rax, -1			; if whitespace - end of word (= end of stream)
	je	.end_of_stream		;
	cmp	rax, ASCII_NULL		; if null - end of stream
	je	.end_of_stream		;
	mov	[rdi], al		; mem[rdi] <- read char
	inc	rdi			; rdi++ to point on null
	dec	rsi			; dec remaining buffer size
	jz	.end_of_buffer		; if buffer is empty - end of buffer
	jmp	.loop			;
.end_of_stream:				;
	cmp	rsi, 1			; if remaining buffer space more than 1 ->
	jge	.success		; -> successfully read word
.end_of_buffer:				;
	pop	rax			; load saved buffer pointer
	xor	rax, rax		; rax <- 0 (fail)
	ret				;
.success:				;
	mov	[rdi], byte ASCII_NULL	; mem[rdi] <- null terminator
	pop	rax			; load saved buffer pointer
	mov	rdx, rdi		; rdx <- pointer to the end of string
	sub	rdx, rax		; rdx-=rax (compute read string length)
	ret				;


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	; rdi - char* string
	mov	rax, 0			; rax <- 0 (result of multiply)
	mov	r9, 0			; r9 <- 0 (length of parsed string)
	mov	rsi, 0			; rsi <- 0 (current char)
	mov	r8, 10			; r8 <- base of multiply (10)
.loop:					;
	mov	sil, [rdi]		; sil <- current char
	inc	rdi			; inc string pointer
	cmp	rsi, ASCII_NULL		; if null - end of number
	je	.end			;
	sub	rsi, ASCII_ZERO		; get the ordinal of char
	cmp	rsi, 9			; check if char is between zero and nine ->
	ja	.end			; -> if not in range - end of number
	mul	r8			; rax = rax * 10 + rsi
	add	rax, rsi		;
	inc	r9			; inc length of number
	jmp	.loop			;
.end:					;
	mov	rdx, r9			; rdx <- r9 (number length)
	ret				;


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	; rdi - char* string
	cmp	byte[rdi], ASCII_MINUS	; if positive - parse_uint
	jne	parse_uint		;
	inc	rdi			; inc string pointer
	call	parse_uint		; parse unsignen number
	cmp	rdx, 0			; if 0 len parsed - return (no result)
	je	.end			;
	add	rdx, 1			; add 1 to number length because '-'
	neg	rax			; rax = - rax (neg fix)
.end:					;
	ret				;


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер (копировать нужно даже если строка не умещается???? будем считать да)
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	; rdi - char* string
	; rsi - void* buffer
	; rdx - buffer length
	cmp	rdx, 0			; if buffer is empty - return
	je	.end_of_buffer		; funny :>
	mov	rax, 0			; rax <- 0 (string length)
.loop:					;
	mov	r8b, [rdi + rax]	; move char from string to buffer
	mov	[rsi + rax], r8b	; powered by r8b (cannot 'mov mem[], mem[]')
	inc	rax			; inc string length
	dec	rdx			; dec remaining buffer size
	cmp	r8b, ASCII_NULL		; if copied char is null - end of string
	je	.end_of_string		;
	cmp	rdx, 0			; if remaining space is null - end of buffer
	je	.end_of_buffer		;
	jmp	.loop			;
.end_of_buffer:				;
	mov	rax, 0			; rax <- 0 (fail)
.end_of_string:				;
	ret				;
